from tkinter import Tk,Button,Frame,Label,mainloop,Entry,Widget,font
from tkinter.ttk import Progressbar

class Question(Frame):

    def __init__(self,master):
        super(Question,self).__init__(master)

        Label(self,text="Enter your Age").pack(anchor='nw',fill='x', expand=1)

        self.birth=Entry(self,font=font.Font(size=24))
        self.birth.pack(anchor='nw',expand=1,fill='x')
    
    def getBirth(self):
        return self.birth.get()


class Answer(Frame):

    def __init__(self,master):
        super(Answer,self).__init__(master)
    
        Label(self,text='Your age is').pack(anchor='nw',expand=0)
        self.output=Label(self,text="",font=font.Font(size=48))
        self.output.pack(anchor='nw',expand=1,fill='both')
    
    def setBirth(self,birth):
        self.output.configure(text=birth)
        self.master.update()


class Waiting(Frame):
    
    def __init__(self,master):
        super(Waiting,self).__init__(master)
        self.progress=Progressbar(self,orient='horizontal',length=200,mode="determinate",takefocus=True,maximum=100)
        self.progress.pack(anchor='center',expand=1,fill='x')
        Label(self,text='Processing...').pack(anchor='n',expand=1,fill='x')


    def wait(self):
        for i in range(100):
            self.progress.step()
            self.master.update()
            self.master.after(50)

class Main(Frame):
    
    def __init__(self,master):
        super(Main,self).__init__(master)
        self.question=Question(self)
        self.question.pack(expand=1,fill='both')

        self.btn=Button(self,text='Calcular')
        self.btn.bind('<Button-1>',self.handleEvent)
        self.btn.pack(anchor='se')
    
    def handleEvent(self,event):
        
        birth=self.question.getBirth()
        self.question.destroy()
        self.btn.destroy()
        self.waiting=Waiting(self)
        self.waiting.pack(anchor='center',fill='both',expand=1 )
        self.waiting.wait()
        self.waiting.destroy()
        self.answer=Answer(self)
        self.answer.setBirth(birth)
        self.answer.pack(anchor='center',expand=1,fill='both')


def main():
    width=200
    height=200
    tk=Tk()
    tk.configure(borderwidth=10)
    tk.wm_title('Age calculator')
    tk.wm_geometry('%dx%d+%d+%d'%(width,height,(tk.winfo_screenwidth()-width)//2,(tk.winfo_screenheight()-height)//2))
    tkFont=font.nametofont('TkDefaultFont')
    tkFont.config(size=12)
    sc=Main(tk)
    sc.pack(expand=1,anchor='nw',fill='both')

    mainloop()

if __name__ == '__main__':
    main()
